package org.firstinspires.ftc.teamcode.auto;

import android.graphics.Color;
import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.acmerobotics.dashboard.telemetry.MultipleTelemetry;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.ElapsedTime;


import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.teamcode.robot.Arm;
import org.firstinspires.ftc.teamcode.robot.Donuts;
import org.firstinspires.ftc.teamcode.robot.GamepadController;
import org.firstinspires.ftc.teamcode.robot.Intake;
import org.firstinspires.ftc.teamcode.robot.Launcher;
import org.firstinspires.ftc.teamcode.robot.Movement;
import org.firstinspires.ftc.teamcode.robot.Position;
import org.firstinspires.ftc.teamcode.robot.Position2;

import com.qualcomm.robotcore.util.Range;

import static java.lang.Thread.sleep;

@Autonomous(name="AutoOpMode 1")
@Config
public class AutoOpMode extends LinearOpMode
{
    public static double speed = 0.6;
    public static double sideSpeed = 0.8;
    public static double turnSpeed = 0.5;
    public static int lineDetect = 16;
    public static int adjustAmount = 3;
    public static int TEST_VAR = 3;

    //////////////////////////////////////////////////////// CLASS MEMBERS /////////////////////////////////////////////////////////

    // Elapsed game time tracking.
    private FtcDashboard dashboard = FtcDashboard.getInstance();
    private ElapsedTime runtime = new ElapsedTime();
    private Movement movement;
    private Arm arm;
    private Intake intake;
    private Launcher launcher;
    private Position2 position;
    private GamepadController gamepad;
    private Donuts donuts;

    // Variables storing last state of gamepad buttons
    private boolean lastA = false;
    private boolean lastB = false;
    private boolean lastX = false;
    private boolean lastY = false;
    private double lastXtime = 0.0;
    private double lastRuntime = 0.0;

    private boolean tempIntake = false;

    private double tempMotorSpeed = 0.0;
    private boolean lastLT = false;
    private boolean lastRT = false;
    private boolean lastRB = false;

    private float hsvValues1[] = { 0F, 0F, 0F };
    private float hsvValues2[] = { 0F, 0F, 0F };

    private double dpadTime;

    public enum Team {
        BLUE,
        RED
    }
    public Team team = Team.BLUE;

    private ColorSensor colorSensor1;
    private ColorSensor colorSensor2;

    public enum Path {
        A,
        B,
        C
    }

    public void scanColor1() {
        Color.RGBToHSV(
                (int) (colorSensor1.red() * 255),
                (int) (colorSensor1.green() * 255),
                (int) (colorSensor1.blue() * 255),
                hsvValues1
        );
    }
    public void scanColor2() {
        Color.RGBToHSV(
                (int) (colorSensor2.red() * 255),
                (int) (colorSensor2.green() * 255),
                (int) (colorSensor2.blue() * 255),
                hsvValues2
        );
    }

    public void move(int amount, double x, double y, double r) {
        int start = movement.getEncoder();
        movement.move(x,y,r);
        movement.apply();
        while (Math.abs(movement.getEncoder() - start) < amount) {
            if (!opModeIsActive()) {
                break;
            }
        }
        movement.reset();
        movement.apply();
    }

    public void sleepFor(double millis) {
        telemetry.addData("Sleeping for ", millis);
        telemetry.update();
        double startTime = runtime.time();
        while (runtime.time() - startTime < (millis / 1000)) {
            if (!opModeIsActive()) {
                break;
            }
        }
    }

    ///////////////////////////////////////////////////////// OPMODE METHODS /////////////////////////////////////////////////////////
    @Override
    public void runOpMode() {
        //////////// INIT ///////////
        telemetry = new MultipleTelemetry(telemetry, dashboard.getTelemetry());

        telemetry.addData("Initializing...", "");

        colorSensor1 = hardwareMap.get(ColorSensor.class, "color_sensor_1");
        colorSensor2 = hardwareMap.get(ColorSensor.class, "color_sensor_2");

        movement = new Movement(
                telemetry,
                runtime,
                hardwareMap.get(DcMotor.class, "front_left_drive"),
                hardwareMap.get(DcMotor.class, "front_right_drive"),
                hardwareMap.get(DcMotor.class, "back_left_drive"),
                hardwareMap.get(DcMotor.class, "back_right_drive")
        );

        telemetry.addData("Movement", "Initialized");

        arm = new Arm(
                telemetry,
                hardwareMap.get(Servo.class, "arm_servo"),
                hardwareMap.get(Servo.class, "grip_servo")
        );

        telemetry.addData("Arm", "Initialized");

        intake = new Intake(
                telemetry,
                hardwareMap.get(DcMotor.class, "intake_motor")
        );

        telemetry.addData("Intake", "Initialized");

        launcher = new Launcher(
                telemetry,
                runtime,
                hardwareMap.get(DcMotor.class, "launcher_1"),
                hardwareMap.get(DcMotor.class, "launcher_2"),
                hardwareMap.get(Servo.class, "tilt_servo"),
                hardwareMap.get(DcMotor.class, "belt_motor")
        );

        telemetry.addData("Launcher", "Initialized");

        position = new Position2(
                telemetry,
                null,
                null,
                null
        );

        telemetry.addData("Position", "Initialized");

        donuts = new Donuts(
                telemetry,
                runtime,
                hardwareMap.get(TouchSensor.class, "donut_entry"),
                hardwareMap.get(TouchSensor.class, "donut_exit"),
                hardwareMap.get(WebcamName.class, "Webcam 1"),
                hardwareMap.appContext.getResources().getIdentifier("tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName())
        );

        telemetry.addData("Robot Ready", "");

        telemetry.update();

        arm.setGrip(false);


        waitForStart();
        //////////// RUN /////////////
        // Init
        telemetry.addData("Step", "starting");
        telemetry.update();
        arm.setArm(true);
        arm.setGrip(false);
        donuts.activate();

        move(500, -speed, 0, 0);

        // Detect donuts
        Path path = null;
        Donuts.CameraDonuts donut = Donuts.CameraDonuts.UNSURE;
        double startCheckTime = runtime.time();
        while ((donut == Donuts.CameraDonuts.NONE || donut == Donuts.CameraDonuts.UNSURE) && (runtime.time() - startCheckTime < 1)) {
            telemetry.addData("Step", "checking donuts");
            telemetry.addData("Seeing donuts", donut);
            telemetry.update();
            donut = donuts.checkCamera();
            if (!opModeIsActive()) return;
        }

        intake.setIntake(true);
        intake.apply();
        sleepFor(1000); // TODO: Replace with while loop and runtime
        intake.setIntake(false);
        intake.apply();

        telemetry.addData("Step", "Going to position");
        telemetry.addData("Saw donuts", donut);
        telemetry.update();

        switch (donut) {
            case SINGLE:
                path = Path.B;
                break;
            case QUAD:
                path = Path.C;
                break;
            default:
                path = Path.A;
                break;
        }

        // Go to square
        move(1000, -speed, 0, 0);

        move(1500, 0, sideSpeed, 0);

        move(50, 0, 0, -turnSpeed);

        movement.reset();
        movement.apply();

        scanColor2();
        do {
            scanColor2();
            move(lineDetect, -speed, 0, 0);
            if (!opModeIsActive()) return;
        } while (hsvValues2[2] < 7000.0);

        movement.reset();
        movement.apply();
        sleepFor(500);

        switch (path) {
            case A:
                move(300, speed, 0, 0);
                break;
            case B:
                move(500, -speed, 0, 0);
                move(2000, 0, -sideSpeed, 0);
                break;
            case C:
                move(1400, -speed, 0, 0);
                break;
        }

        telemetry.addData("Step", "Putting down wobble");
        telemetry.update();

        // Let go of wobble
        arm.setArm(false);
        sleepFor(1000);
        arm.setGrip(true);
        sleepFor(500);
        arm.setArm(true);

        // Go back
        if (path == Path.B) {
            move(1500, 0, sideSpeed, 0);
        }

        if (path != Path.A) {
            movement.reset();
            movement.apply();

            scanColor1();
            scanColor2();
            // Move back until line and a little more
            do {
                scanColor1();
                scanColor2();
                move(lineDetect*2, speed, 0, 0);
                if (!opModeIsActive()) return;
            } while (hsvValues2[2] < 7000.0 && hsvValues1[2] < 7000.0);
            movement.reset();
            movement.apply();
            sleepFor(500);

            move(200, speed, 0, 0);
        }

        // Move against wall to re-align
        move(900, 0, sideSpeed, 0);

        // move(50, 0, 0, turnSpeed);

        // Move to launch position
        move(1500, 0, -sideSpeed, 0);

        scanColor1();
        scanColor2();
        do {
            scanColor1();
            scanColor2();
            move(lineDetect/2, -speed, 0, 0);
            if (!opModeIsActive()) return;
        } while (hsvValues1[2] < 7000.0 && hsvValues2[2] < 7000.0);
        if (hsvValues1[2] > 7000.0) {
            while (hsvValues2[2] < 7000.0) {
                scanColor2();
                // scanColor1();
                telemetry.addData("1 was on white, value 2", hsvValues2[2]);
                telemetry.addData("value 1", hsvValues1[2]);
                telemetry.update();
                move(adjustAmount, 0, 0, turnSpeed);
                if (!opModeIsActive()) return;
                // if (hsvValues1[2] < 7000.0) {
                //     move(adjustAmount, -speed, 0, 0);
                // }
            }
            move(5, 0, 0, turnSpeed);
        } else {
            while (hsvValues1[2] < 7000.0) {
                scanColor1();
                scanColor2();
                telemetry.addData("2 was on white, value 1", hsvValues1[2]);
                telemetry.addData("value 2", hsvValues2[2]);
                telemetry.update();
                move(adjustAmount, 0, 0, -turnSpeed);
                if (!opModeIsActive()) return;
                // if (hsvValues2[2] < 7000.0) {
                //     move(adjustAmount, -speed, 0, 0);
                // }
            }
            move(5, 0, 0, -turnSpeed);
        }

        move(100, speed, 0, 0);

        telemetry.addData("Step", "launching donuts");
        telemetry.update();

        // Launch all three
        launcher.setTarget(Launcher.Target.HIGH_GOAL_BLUE);
        launcher.setLauncher(true);
        sleepFor(100);
        launcher.setBelt(true);
        launcher.apply();
        double startTime = runtime.time();
        while (runtime.time() - startTime < 5.0) {
            launcher.apply();
            telemetry.update();
            if (!opModeIsActive()) return;
        }
        launcher.setLauncher(false);
        launcher.setBelt(false);
        launcher.apply();

        telemetry.addData("Step", "Parking");
        telemetry.update();

        // Go to line
        move(300, -speed, 0, 0);
        telemetry.addData("Step", "done");
        telemetry.update();
    }


}
