package org.firstinspires.ftc.teamcode.manual;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.telemetry.MultipleTelemetry;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.ElapsedTime;


import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.teamcode.robot.Arm;
import org.firstinspires.ftc.teamcode.robot.Donuts;
import org.firstinspires.ftc.teamcode.robot.GamepadController;
import org.firstinspires.ftc.teamcode.robot.Intake;
import org.firstinspires.ftc.teamcode.robot.Launcher;
import org.firstinspires.ftc.teamcode.robot.Movement;
import org.firstinspires.ftc.teamcode.robot.Position;
import org.firstinspires.ftc.teamcode.robot.Position2;

import com.qualcomm.robotcore.util.Range;

import static java.lang.Thread.sleep;

@TeleOp(name="Manual")
public class ManualOpMode extends OpMode
{
    public boolean motorsEnabled = true;
    public enum Team {
        BLUE,
        RED
    }
    public Team team = Team.BLUE;
    //////////////////////////////////////////////////////// CLASS MEMBERS /////////////////////////////////////////////////////////

    // Elapsed game time tracking.
    private FtcDashboard dashboard = FtcDashboard.getInstance();
    private ElapsedTime runtime = new ElapsedTime();
    private Movement movement;
    private Arm arm;
    private Intake intake;
    private Launcher launcher;
    private Position2 position;
    private GamepadController gamepad;
    private Donuts donuts;

    // Variables storing last state of gamepad buttons
    private boolean lastA = false;
    private boolean lastB = false;
    private boolean lastX = false;
    private boolean lastY = false;
    private double lastXtime = 0.0;
    private double lastRuntime = 0.0;

    private boolean tempIntake = false;

    private double tempMotorSpeed = 0.0;
    private boolean lastLT = false;
    private boolean lastRT = false;
    private boolean lastRB = false;

    private double dpadTime;

    public void setMotorsEnabled(boolean val) {
        motorsEnabled = val;
    }

    ///////////////////////////////////////////////////////// OPMODE METHODS /////////////////////////////////////////////////////////
    @Override
    public void init() {
        telemetry = new MultipleTelemetry(telemetry, dashboard.getTelemetry());

        telemetry.addData("Initializing...", "");

        movement = new Movement(
                telemetry,
                runtime,
                hardwareMap.get(DcMotor.class, "front_left_drive"),
                hardwareMap.get(DcMotor.class, "front_right_drive"),
                hardwareMap.get(DcMotor.class, "back_left_drive"),
                hardwareMap.get(DcMotor.class, "back_right_drive")
        );

        telemetry.addData("Movement", "Initialized");

        gamepad = new GamepadController(
          telemetry,
          runtime,
          gamepad1
        );

        telemetry.addData("Gamepad", "Initialized");

        arm = new Arm(
                telemetry,
                hardwareMap.get(Servo.class, "arm_servo"),
                hardwareMap.get(Servo.class, "grip_servo")
        );

        telemetry.addData("Arm", "Initialized");

        intake = new Intake(
                telemetry,
                hardwareMap.get(DcMotor.class, "intake_motor")
        );

        telemetry.addData("Intake", "Initialized");

        launcher = new Launcher(
                telemetry,
                runtime,
                hardwareMap.get(DcMotor.class, "launcher_1"),
                hardwareMap.get(DcMotor.class, "launcher_2"),
                hardwareMap.get(Servo.class, "tilt_servo"),
                hardwareMap.get(DcMotor.class, "belt_motor")
        );

        telemetry.addData("Launcher", "Initialized");

        position = new Position2(
                telemetry,
                null,
                null,
                null
        );

        telemetry.addData("Position", "Initialized");

        donuts = new Donuts(
                telemetry,
                runtime,
                hardwareMap.get(TouchSensor.class, "donut_entry"),
                hardwareMap.get(TouchSensor.class, "donut_exit"),
                hardwareMap.get(WebcamName.class, "Webcam 1"),
                hardwareMap.appContext.getResources().getIdentifier("tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName())
        );

        telemetry.addData("Robot Ready", "");

        telemetry.update();
    }

    @Override
    public void init_loop() {
    }

    @Override
    public void start() {
        runtime.reset();
    }
    ///////////////////////////////////////////////////////// MAIN LOOP /////////////////////////////////////////////////////////
    @Override
    public void loop() {
        /* MOVEMENT STUFF */
        movement.reset();

        telemetry.addData("Motors", motorsEnabled ? "enabled" : "disabled");
        if (motorsEnabled) {
            // Translation : joystick (fast) and dpad (slow)
            movement.joystickTranslate(gamepad1);
            movement.dpadTranslate(gamepad1);
            // Rotation : bumpers (fast) and triggers (slow)
            movement.bumperTurn(gamepad1);

            // Actual moving
            movement.apply();
        }

        /* TOGGLE MOTORS & SERVOS */
        // Change speed of launchers depending on selection
        launcher.setTargetGamepad(gamepad1);
        // A to toggle grip state
        if (gamepad.press(GamepadController.Button.A)) arm.toggleGrip();
        // B to toggle rest of arm
        if (gamepad.press(GamepadController.Button.B)) arm.toggleArm();
        // X to toggle donut intake
        if (gamepad.press(GamepadController.Button.X)) intake.toggleIntake();
        // if (gamepad.longPress(gamepad.X)) intake.setDirection(false) else intake.setDirection(true);
        // Y to toggle launcher motors
        if (gamepad.press(GamepadController.Button.Y) || gamepad.press(GamepadController.Button.RIGHT_STICK)) launcher.toggle();
        if (gamepad.longPress(GamepadController.Button.Y)) launcher.reverseBelt();


        // Apply motors & servos
        intake.apply();
        launcher.apply();

        /* POSITION & STATE */
        // Update
        VectorF pos = position.update();
        int numDonuts = donuts.update();
        // Print
        telemetry.addData("Donuts", numDonuts);
        telemetry.addData("Position", "{X, Y, Z} = %.1f, %.1f, %.1f",
                pos.get(0), pos.get(1), pos.get(2));


        /* CHORES */
        lastRuntime = runtime.time();

        telemetry.update();

    }

    ///////////////////////////////////////////////////////// STOP METHOD /////////////////////////////////////////////////////////
    @Override
    public void stop() {
    }
}
