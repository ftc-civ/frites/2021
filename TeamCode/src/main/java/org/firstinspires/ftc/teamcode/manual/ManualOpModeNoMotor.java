package org.firstinspires.ftc.teamcode.manual;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

@TeleOp(name="Manual No Motor")
public class ManualOpModeNoMotor extends ManualOpMode
{
    ManualOpModeNoMotor() {
        setMotorsEnabled(false);
    }
}
