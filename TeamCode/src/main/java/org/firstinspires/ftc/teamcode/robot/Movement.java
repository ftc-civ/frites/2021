/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode.robot;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.hardware.Gamepad;

import org.firstinspires.ftc.robotcore.external.Telemetry;

@Config
public class Movement
{
    public static double frontLeftCoeff = 1;
    public static double frontRightCoeff = 1;
    public static double backLeftCoeff = 1;
    public static double backRightCoeff = 1;
    public static double turnSensitivity = 2; // Increasing sensitivity gives more priority to turning
    public static double frontSensitivity = 1;
    public static double sidewaysSensitivity = 1;
    public static double maxDpadSensitivity = 0.4; // How much the robot moves when dpad is used
    public static double timeToDpadFront = 3;
    public static double timeToDpadSideways = 2;
    public static double dpadMarginFront = 0.2;
    public static double dpadMarginSideways = 0.3;

    private float turn = 0f;

    private ElapsedTime runtime;

    // Initialize motors
    private DcMotor frontLeftDrive, frontRightDrive, backLeftDrive, backRightDrive, ticksMotor;

    private double frontLeftPower = 0;
    private double frontRightPower = 0;
    private double backLeftPower = 0;
    private double backRightPower = 0;

    private Telemetry telemetry;

    private double firstTurnTicks;
    private boolean rotateLock;
    private double ticksAfterFullRotation = 500; // TODO: Measure this with
    private double turnAngle, turnSpeed; // Angle in degrees

    private double dpadTime; // The time the dpad has been pressed, to progressively ramp up speed

    public Movement(Telemetry globalTelemetry, ElapsedTime globalRuntime, DcMotor FL, DcMotor FR, DcMotor BL, DcMotor BR) {

        // INITIALIZE TELEMETRY
        telemetry = globalTelemetry;

        runtime = globalRuntime;

        // Assign motors from ManualOpMode
        frontLeftDrive  = FL; frontLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        frontRightDrive = FR; frontRightDrive.setDirection(DcMotor.Direction.REVERSE);
        backLeftDrive  = BL;  backLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        backRightDrive = BR;  backRightDrive.setDirection(DcMotor.Direction.REVERSE);

        ticksMotor = backLeftDrive;
    }

    /* MOVEMENT CALCULATIONS */

    // Move in a direction indicated by the first two coordinates, and turn
    public void move(double front, double sideways, double turn) {
        backLeftPower  += front + sideways*backLeftCoeff - turn;
        backRightPower += front - sideways*backRightCoeff + turn;
        frontRightPower  += front + sideways*frontRightCoeff + turn; // ou 0.2?
        frontLeftPower  += front - sideways*frontLeftCoeff - turn;
    }

    // Move with an angle and a speed instead
    public void polarMove(double angle, double speed, double turn) {
        move(
                Math.sin(angle) * speed,
                Math.cos(angle) * speed,
                turn
        );
    }

    // Add extra rotation
    public void rotate(double angleInDegrees, double speed) {
        rotateLock = true;
        firstTurnTicks = ticksMotor.getCurrentPosition();
        turnAngle = angleInDegrees; turnSpeed = speed;
    }

    // Add rotation lock
    public void keepRotating() {
        if (turnAngle > 0) {
            if (firstTurnTicks + turnAngle / 360 * ticksAfterFullRotation > ticksMotor.getCurrentPosition()) {
                move(0, 0, turnSpeed);
            } else
                rotateLock = false;
        } else {
            if (firstTurnTicks - turnAngle / 360 * ticksAfterFullRotation < ticksMotor.getCurrentPosition()) {
                move(0, 0, -turnSpeed);
            } else
                rotateLock = false;
        }
    }

    // Check if rotation lock is enabled
    public boolean checkRotating() {
        return rotateLock;
    }

    // Turns off all motors, full stop
    public void reset(boolean includeTurn) {
        frontLeftPower = 0;
        frontRightPower = 0;
        backLeftPower = 0;
        backRightPower = 0;
        if (includeTurn)
            turn = 0f;
    }
    public void reset() {
        reset(true);
    }

    /* GAMEPAD CONTROL */

    // Translates the robot with the left joystick
    public void joystickTranslate(Gamepad gamepad) {
        double sideways = gamepad.left_stick_x;
        double front = gamepad.left_stick_y;
        // Uncomment for gradual speed increase
        if (Math.abs(sideways) < 0.1) {
            sideways = 0;
        } else if (Math.abs(sideways) > 0.1 || Math.abs(sideways) < 0.7) {
            sideways = 0.83*sideways+0.02;
        } else if (Math.abs(sideways) >= 0.7 || Math.abs(sideways) < 0.9) {
            sideways = 2*sideways-0.8;
        } else if (Math.abs(sideways) >=0.9) {
            sideways = 1;
        } else {
            sideways = 0;
        }

        if (Math.abs(front) < 0.1) {
            front = 0;
        } else if (Math.abs(front) > 0.1 || Math.abs(front) < 0.7) {
            front = 0.83*front+0.02;
        } else if (Math.abs(front) >= 0.7 || Math.abs(front) < 0.9) {
            front = 2*front-0.8;
        } else if (Math.abs(front) >=0.9) {
            front = 1;
        } else {
            front = 0;
        }
        move(
                front,
                sideways,
                0
        );
    }

    // Move slowly with the dpad
    public void dpadTranslate(Gamepad gamepad) {
        if (!(gamepad.dpad_up || gamepad.dpad_down || gamepad.dpad_left || gamepad.dpad_right)) {
          dpadTime = runtime.time();
        } // Allows us to get the time the dpad was pressed, so we can progressively augment the speed of the robot
        double time = runtime.time() - dpadTime;
        double sidewaysTime = time/timeToDpadSideways;
        double frontTime = time/timeToDpadFront;
        sidewaysTime += dpadMarginSideways;
        frontTime += dpadMarginFront;
        if (sidewaysTime > maxDpadSensitivity) sidewaysTime = maxDpadSensitivity;
        if (frontTime > maxDpadSensitivity) frontTime = maxDpadSensitivity;
        move(
                (gamepad.dpad_down? frontTime :0) - (gamepad.dpad_up? frontTime :0),
                (gamepad.dpad_right? sidewaysTime :0) - (gamepad.dpad_left? sidewaysTime :0),
                0
        );
    }

    // Turn with the bumpers and triggers
    public void bumperTurn(Gamepad gamepad) {
        if (gamepad.left_bumper || gamepad.right_bumper) {
            if (gamepad.left_bumper)
                turn -= 0.3;
            if (gamepad.right_bumper)
                turn += 0.3;
        } else {
            turn += gamepad.right_trigger*0.8;
            turn -= gamepad.left_trigger*0.8;
        }
        if (turn != 0) {
            reset(false); // Because Jeremy wanted to
        }
        move(0, 0, turn);
    }

    /* GET INFO */

    public int getEncoder() {
        return frontLeftDrive.getCurrentPosition();
    }

    /* APPLY CHANGES */

    // Apply all changes made before
    public void apply() {
        frontLeftDrive.setPower(Range.clip(frontLeftPower, -1.0, 1.0));
        frontRightDrive.setPower(Range.clip(frontRightPower, -1.0, 1.0));
        backLeftDrive.setPower(Range.clip(backLeftPower, -1.0, 1.0));
        backRightDrive.setPower(Range.clip(backRightPower, -1.0, 1.0));

        // telemetry.addData("Front", "left (%.2f), right (%.2f)", frontLeftPower, frontRightPower);
        // telemetry.addData("Right", "left (%.2f), right (%.2f)", backLeftPower, backRightPower);
    }
}
