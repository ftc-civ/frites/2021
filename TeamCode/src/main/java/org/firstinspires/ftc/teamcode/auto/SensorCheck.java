package org.firstinspires.ftc.teamcode.auto;

import android.graphics.Color;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.telemetry.MultipleTelemetry;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.teamcode.robot.Arm;
import org.firstinspires.ftc.teamcode.robot.Donuts;
import org.firstinspires.ftc.teamcode.robot.GamepadController;
import org.firstinspires.ftc.teamcode.robot.Intake;
import org.firstinspires.ftc.teamcode.robot.Launcher;
import org.firstinspires.ftc.teamcode.robot.Movement;
import org.firstinspires.ftc.teamcode.robot.Position2;

@Autonomous(name="SensorCheck 1")
public class SensorCheck extends LinearOpMode
{
    //////////////////////////////////////////////////////// CLASS MEMBERS /////////////////////////////////////////////////////////

    // Elapsed game time tracking.
    private FtcDashboard dashboard = FtcDashboard.getInstance();
    private ElapsedTime runtime = new ElapsedTime();

    // Variables storing last state of gamepad buttons
    private boolean lastA = false;
    private boolean lastB = false;
    private boolean lastX = false;
    private boolean lastY = false;
    private double lastXtime = 0.0;
    private double lastRuntime = 0.0;

    private boolean tempIntake = false;

    private double tempMotorSpeed = 0.0;
    private boolean lastLT = false;
    private boolean lastRT = false;
    private boolean lastRB = false;

    private float hsvValues[] = { 0F, 0F, 0F };

    private double dpadTime;

    public enum Team {
        BLUE,
        RED
    }
    public Team team = Team.BLUE;

    private ColorSensor colorSensor;

    public enum Path {
        A,
        B,
        C
    }

    public void scanColor() {
        Color.RGBToHSV(
                (int) (colorSensor.red() * 255),
                (int) (colorSensor.green() * 255),
                (int) (colorSensor.blue() * 255),
                hsvValues
        );
    }

    ///////////////////////////////////////////////////////// OPMODE METHODS /////////////////////////////////////////////////////////
    @Override
    public void runOpMode() {
        colorSensor = hardwareMap.get(ColorSensor.class, "color_sensor");

        waitForStart();

        while (opModeIsActive()) {
            scanColor();

            telemetry.addData("Checking line, brightness:", hsvValues[2]);
            telemetry.addData("Checking hue:", hsvValues[0]);
            telemetry.addData("Checking saturation:", hsvValues[1]);
            telemetry.update();
        }
    }


}
