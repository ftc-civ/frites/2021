/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


 /*
----------------------------
Cette classe stocke la position du robot et permet de la mettre à jour, en utilisant Vuforia ou possiblement d'autres méthodes.

Elle permet aussi de calculer les déplacements pour aller à une certaine position
 */

package org.firstinspires.ftc.teamcode.robot;

import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;

public class Position2
{

    private Telemetry telemetry;

    private VectorF position = new VectorF(0.0f, 0.0f, 0.0f); // X, Y, angle

    public Position2(Telemetry globalTelemetry, DcMotor encoderLeft, DcMotor encoderRight, DcMotor encoderFront) {
        // INITIALIZE TELEMETRY
        telemetry = globalTelemetry;
    }

    /* POSITION TRACKING */
    // Updates the current position of the robot using whatever method is available
    public VectorF update() {
        return position;
    }

    public VectorF get() {
      return position;
    }

    /* MOVEMENT CONTROL */
    // Renvoie la direction (en polarMove) que le robot devrait suivre pour arriver au point target, sous la forme d'un Vecteur (X, Y, rotation)
    public VectorF getDirection(VectorF target) {
        return new VectorF(0.0f, 0.0f, 0.0f);
    }
}
